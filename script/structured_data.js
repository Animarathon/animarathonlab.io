{
 "@context": "http://schema.org",
 "@type": "Event",
 "name": "Animarathon XV",
 "startDate": "2017-03-25T9:00",
 "doorTime": "2017-03-25T9:00",
 "endDate": "2017-03-25T23:59",
 "url": "http://animarathon.com/animarathon.html",
 "image": "http://animarathon.com/media/media_nonfree/sisters.png",
 "typicalAgeRange": "Everyone",
 "location": [{
  "@type": "Place",
  "sameAs": "https://www.bgsu.edu/bowen-thompson-student-union.html",
  "name": "The Bowen Thompson Student Union",
  "address": [{
   "streetAddress": "Bowen Thompson Student Union, Bowling Green State University", 
   "addressLocality": "Bowling Green", 
   "addressRegion": "OH",
   "postalCode": "43404"
   }]
 }]

}
